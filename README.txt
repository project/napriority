This module restores the Priority feature of Node Access that was removed
in Drupal 8.

Node Access Priority has no user interface and no functionality on its own.
If you use multiple Node Access modules, then those that support Priority
will let you increase or decrease their priority relative to the others.
