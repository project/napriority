<?php

/**
 * @file
 * A helper module to manage priority of Node Access modules.
 *
 * The Drupal 7 'priority' value was removed from hook_node_access_records()
 * for Drupal 8. This module puts it back in.
 */

/**
 * Implements hook_node_access_records_alter().
 *
 * Look for 'priority' values in the collected node access records and keep
 * only the records with the highest priority. Records without 'priority' are
 * assumed to have a default priority of 0.
 *
 * @param $grants
 *   The $grants array returned by hook_node_access_records().
 * @param Drupal\node\Plugin\Core\Entity\Node $node
 *   The node for which the grants were acquired.
 *
 * @see hook_node_access_records_alter()
 * @see hook_node_access_records()
 * @ingroup node_access
 */
function napriority_node_access_records_alter(&$grants, Drupal\node\Plugin\Core\Entity\Node $node) {
  if (!empty($grants)) {
    // Retain grants by highest priority.
    $grants_by_priority = array();
    foreach ($grants as $g) {
      $g += array('priority' => 0);
      $grants_by_priority[intval($g['priority'])][] = $g;
    }
    krsort($grants_by_priority);
    $grants = array_shift($grants_by_priority);
  }
}